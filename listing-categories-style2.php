<?php
    session_start();

    require 'db.php';
    require 'Category.php';
    $cat = new Category;
    $list = $cat->getList($con);
    $message='';
    $errorMessage='';
    if (isset($_SESSION['message'])) {
        $errorMessage = $_SESSION['message'];
        $_SESSION['message']='';
    }
    if (isset($_SESSION['userDetails'])) {
        $message='Success';
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Rental Express</title>
        <link rel="shortcut icon" href="dist/images/favicon.ico">
        <!--Plugin CSS-->
        <link href="dist/css/plugins.min.css" rel="stylesheet">
        <!--main Css-->
        <link href="dist/css/main.min.css" rel="stylesheet"> 
    </head>

    <body>
        <!-- header -->
        <div id="header-fix" class="header fixed-top">
            <nav class="navbar navbar-toggleable-md navbar-expand-lg navbar-light py-lg-0 py-4">
                <a class="navbar-brand mr-4 mr-md-5" href="index.php"><img src="dist/images/logo-v1.png" alt=""></a>
                <div id="dl-menu" class="dl-menuwrapper d-block d-lg-none float-right">
                    <button>Open Menu</button>
                    <ul class="dl-menu">

                        <li class="nav-item active">
                            <a class="nav-link" href="index.php" aria-expanded="false">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="listing-categories-style2.php" aria-expanded="false">Explore</a>
                        </li>                        
                        <?php
                            if ($message=='Success') {
                        ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Pages</a>
                            <ul class="dl-submenu">
                                <li class="dl-back"><a href="#">back</a></li>
                            <li><a href="user-profile.php"> User Profile</a></li>
                            <li><a href="log-out.php" class="fa fa-sign-out">Log-out</a></li>
                            </ul>
                        </li>
                        <?php
                            }
                        ?>
                            


                <?php
                    if ($message!='Success') {
                ?>

                    <ul class="list-unstyled my-2 my-lg-0">
                        <li>
                             <a href="loginhead.php" class="text-white"><i class="fa fa-lock pr-2"></i> Sign In</a>
                        </li>
                    </ul>
                     
                <?php
                    }
                else{
                ?> 

                    <ul class="list-unstyled my-2 my-lg-0">
                        <li>
                             <a href="user-profile.php" class="text-white"><i class="fa fa-user-circle-o pr-2"></i>Welcome <?php echo$_SESSION['userDetails']['userName'];?></a>
                        </li>
                    </ul>
                        <li> <a href="add-place-listing.php" ><i class="fa fa-plus pr-1"></i> Add Listing</a></li>
                <?php
                    }
                ?>





                    </ul>
                </div>

                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item"> <a class="nav-link" href="index.php" aria-expanded="false">Home</a>
                        <li class="nav-item active"> <a class="nav-link" href="listing-categories-style2.php" aria-expanded="false">Explore</a>
                        </li>
                        
                        <?php
                            if ($message=='Success') {
                        ?>
                        <li class="nav-item dropdown"> <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Pages <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                            <li><a href="user-profile.php"> User Profile</a></li>
                            <li><a href="log-out.php" class="fa fa-sign-out">Log-out</a></li>
                            </ul>
                        </li>
                        <?php
                            }
                        ?>
                            
                    </ul>
                <?php
                    if ($message!='Success') {
                ?>

                    <ul class="list-unstyled my-2 my-lg-0">
                        <li>
                             <a href="loginhead.php" class="text-white"><i class="fa fa-lock pr-2"></i> Sign In</a>
                        </li>
                    </ul>
                     
                <?php
                    }
                else{
                ?> 

                    <ul class="list-unstyled my-2 my-lg-0">
                        <li>
                             <a href="user-profile.php" class="text-white"><i class="fa fa-user-circle-o pr-2"></i>Welcome <?php echo$_SESSION['userDetails']['userName'];?></a>
                        </li>
                    </ul>
                                        <a href="add-place-listing.php" class="btn btn-outline-light btn-sm ml-0 ml-lg-4 mt-3 mt-lg-0"><i class="fa fa-plus pr-1"></i> Add Listing</a> </div>
                <?php
                    }
                ?>
            </nav>
        </div>
        <!--End header -->
        <!-- Category -->
        <section>
            <div class="container pt-5 mt-5">
                <div class="row justify-content-center">
                    <div class="col-12 col-md-10 text-center">
                        <div class="heading pb-4">
                            <h5 class="lis-light">Find the best places</h5>
                            <h2 class="f-weight-500">Find Listings by Category</h2> </div>
                    </div>
                </div>
                <div class="row">
        <?php
            while ($row=mysqli_fetch_array($list)) {
        ?>
                    <div class="col-12 col-sm-6 col-xl-3 mb-4 wow fadeInUp">
                        <div class="card lis-brd-light text-center text-lg-left lis-info lis-relative">
                            <a href="<?php echo "listing-no-map-fullwidth.php?category=".$row['name'];?>">
                                <div class="lis-grediant grediant-tb-light lis-relative modImage rounded">
                                    <img src="<?php echo $row['imagePath'];?>" alt="<?php echo $row['name'];?>" class="img-fluid rounded" />
                                </div>
                                <div class="lis-absolute lis-left-20 lis-top-20 lis-bg6 lis-icon lis-rounded-circle-50 text-center">
                                    <div class="text-white mb-0 lis-line-height-2_5 h4">
                                        <i class="icofont <?php echo $row['icon'];?>"></i>
                                    </div>
                                </div>
                            </a>
                            <div class="hover-text lis-absolute lis-left-20 lis-bottom-20 lis-font-roboto text-white text-left">
                                <h6 class="text-white mb-0"><?php echo $row['name'];?></h6>
                                <span class="lis-font-roboto"><?php echo $row['listingCount'];?> listing</span>
                            </div>
                        </div>
                    </div>
        <?php
            }
        ?>
                </div>
            </div>
        </section>
        <!--End Category -->
        <!-- Footer-->
<section class="image-bg footer lis-grediant grediant-bt pb-0">
            <div class="background-image-maker"></div>
            <div class="holder-image"> <img src="dist/images/bg3.jpg" alt="" class="img-fluid d-none"> </div>
            <div class="container">
                <div class="row pb-5">
                    <div class="col-12 col-md-8">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                                <h5 class="footer-head">Useful Links</h5>
                                <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li>
                                        <A href="add-place-listing.php"><i class="fa fa-angle-right pr-1"></i> Add Listing</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Contact Us</A>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                                <h5 class="footer-head">My Account</h5>
                                <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li>
                                        <A href="user-profile.php"><i class="fa fa-angle-right pr-1"></i> Dashboard</A>
                                    </li>
                                    <li>
                                        <A href="user-profile.php#listing"><i class="fa fa-angle-right pr-1"></i> My Listing</A>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-md-0">
                                <!-- <h5 class="footer-head">Pages</h5> -->
                                <!-- <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Blog</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Our Partners</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> How It Work</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Privacy Policy</A>
                                    </li>
                                </ul> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="footer-logo">
                            <a href="#"><img src="dist/images/logo-v1.png" alt="" class="img-fluid" /></a>
                        </div>
                        <p class="my-4">Rental Express</p> <a href="#" class="text-white"></a>
                    </div>
                </div>
            </div>
        </section>
        <!--End  Footer-->
        <!-- Top To Bottom-->
        <a href="#" class="scrollup text-center lis-bg-primary lis-rounded-circle-50">
            <div class="text-white mb-0 lis-line-height-1_7 h3"><i class="icofont icofont-long-arrow-up"></i></div>
        </a>
        <!-- End Top To Bottom-->
        <!-- Login /Register Form-->
        <!-- End Login /Register Form-->
        <!-- jQuery -->
        <script src="dist/js/plugins.min.js"></script>
        <script src="dist/js/common.js"></script>

    </body>


</html>