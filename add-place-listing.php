<?php
    session_start();

    require 'db.php';
    require 'Category.php';
    $cat = new Category;
    $list = $cat->getList($con);
    $message='';
    $errorMessage='';
    if (isset($_SESSION['message'])) {
        $errorMessage = $_SESSION['message'];
        $_SESSION['message']='';
    }
    if (isset($_SESSION['userDetails'])) {
        $message='Success';
    }
    if (!isset($_SESSION['userDetails'])) {
        header("location:loginhead.php");
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Rental Express</title>
        <link rel="shortcut icon" href="dist/images/favicon.ico">
        <!--Plugin CSS-->
        <link href="dist/css/plugins.min.css" rel="stylesheet">
        <!--main Css-->
        <link href="dist/css/main.min.css" rel="stylesheet"> 
        <style type="text/css">
            #submit-btn{
                cursor: pointer;
            }
        </style>
    </head>

    <body>
        <!-- header -->
        <div id="header-fix" class="header fixed-top">
            <nav class="navbar navbar-toggleable-md navbar-expand-lg navbar-light py-lg-0 py-4">
                <a class="navbar-brand mr-4 mr-md-5" href="index.php"><img src="dist/images/logo-v1.png" alt=""></a>
                <div id="dl-menu" class="dl-menuwrapper d-block d-lg-none float-right">
                    <button>Open Menu</button>
                    <ul class="dl-menu">

                        <li class="nav-item">
                            <a class="nav-link" href="index.php" data-toggle="dropdown" aria-expanded="false">Home</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="listing-categories-style2.php" aria-expanded="false">Explore</a>
                        </li>
                        <?php
                            if ($message=='Success') {
                        ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Pages</a>
                            <ul class="dl-submenu">
                                <li class="dl-back"><a href="#">back</a></li>
                            <li><a href="user-profile.php"> User Profile</a></li>
                            <li><a href="log-out.php" class="fa fa-sign-out">Log-out</a></li>
                            </ul>
                        </li>
                        <?php
                            }
                        ?>


                <?php
                    if ($message!='Success') {
                ?>

                    <ul class="list-unstyled my-2 my-lg-0">
                        <li>
                             <a href="loginhead.php" class="text-white"><i class="fa fa-lock pr-2"></i> Sign In</a>
                        </li>
                    </ul>
                     
                <?php
                    }
                else{
                ?>
                    <ul class="list-unstyled my-2 my-lg-0">
                        <li>
                             <a href="user-profile.php" class="text-white"><i class="fa fa-user-circle-o pr-2"></i>Welcome <?php echo$_SESSION['userDetails']['userName'];?></a>
                        </li>
                    </ul>

                <?php
                    }
                ?>


                        <li> <a href="add-place-listing.php" ><i class="fa fa-plus pr-1"></i> Add Listing</a></li>



                    </ul>
                </div>

                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active"> <a class="nav-link" href="index.php" aria-expanded="false">Home</a>
                        </li>
                        <li class="nav-item"> <a class="nav-link" href="listing-categories-style2.php" aria-expanded="false">Explore</a>
                        </li>
                        <?php
                            if ($message=='Success') {
                        ?>
                        <li class="nav-item dropdown"> <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Pages <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                            <li><a href="user-profile.php"> User Profile</a></li>
                            <li><a href="log-out.php" class="fa fa-sign-out">Log-out</a></li>
                            </ul>
                        </li>
                        <?php
                            }
                        ?>
                            
                    </ul>

                <?php
                    if ($message!='Success') {
                ?>

                    <ul class="list-unstyled my-2 my-lg-0">
                        <li>
                             <a href="loginhead.php" class="text-white"><i class="fa fa-lock pr-2"></i> Sign In</a>
                        </li>
                    </ul>
                     
                <?php
                    }
                else{
                ?> 
                    <ul class="list-unstyled my-2 my-lg-0">
                        <li>
                             <a href="user-profile.php" class="text-white"><i class="fa fa-user-circle-o pr-2"></i>Welcome <?php echo$_SESSION['userDetails']['userName'];?></a>
                        </li>
                    </ul>

                <?php
                    }
                ?>

                     <a href="add-place-listing.php" class="btn btn-outline-light btn-sm ml-0 ml-lg-4 mt-3 mt-lg-0"><i class="fa fa-plus pr-1"></i> Add Listing</a> </div>
            </nav>
        </div>
        <!--End header -->
        <!-- Page Inner -->
        <section class="lis-bg-light pb-5">
            <div class="container pt-5">
                <div class="row wow fadeInUp">
                    <div class="col-12 col-sm-6">
                        <div class="page-title">
                            <h2>Add Place Listing</h2>
                            <p class="mb-0">Lorem ipsum dolor sit amet, ut latine</p>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 text-left text-sm-right">
                        <ol class="breadcrumb mb-0 pl-0 bg-transparent">
                            <li class="breadcrumb-item d-inline-block float-none"><a href="#" class="lis-light">Home</a></li>
                            <li class="breadcrumb-item d-inline-block float-none active">Add Listing</li>
                        </ol>
                    </div>
                </div>
            </div>
        </section>
        <!--End Page Inner -->
        <!-- Pricing -->
        <section>
            <form action="addlisting.php" method="POST" enctype="multipart/form-data">
                            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-lg-9 mb-5 mb-lg-0">

                        <div class="card lis-brd-light wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                            <div class="card-body p-0">
                                <div class="row p-4">
                                    <div class="col-12 col-md-3">
                                        <div class="form-group lis-relative">
                                            <h6 class="lis-font-weight-500">Have an account?</h6>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-9">
                                        <div class="form-group lis-relative">
                                            Sign in If you don’t have an account you can create one below by entering your email address/username. Your account details will be confirmed via email. <a href="#modal" class="login_form">Sign in</a>
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row p-4">
                                    <div class="col-12 col-sm-12 mb-3">
                                        <h6 class="lis-font-weight-500"><i class="fa fa-tag pr-2 lis-f-14"></i> Categories
                                        </h6>
                                    </div>
                                    <div class="col-12 col-sm-12">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-12 col-md-2 col-form-label">Categories</label>
                                            <div class="col-12 col-md-10">
                                                <select class="form-control border-top-0 border-left-0 border-right-0 rounded-0" id="inlineFormCustomSelect" name="categoryName" required>
                                                    <option selected>Select Category</option>
                                        <?php 
            while ($row=mysqli_fetch_array($list)) {
                                        ?>
                                                    <option value="<?php echo $row['name'];?>"><?php echo $row['name'];?></option>
                                        <?php
                                            }
                                        ?>
                                                </select>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <hr/>

                                <div class="row p-4">
                                    <div class="col-12 col-sm-12 mb-3">
                                        <h6 class="lis-font-weight-500"><i class="fa fa-align-left pr-2 lis-f-14"></i>  Name & Description </h6>

                                    </div>
                                    <div class="col-12 col-sm-12">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-12 col-md-2 col-form-label">Title</label>
                                            <div class="col-12 col-md-10">
                                                <input class="form-control border-top-0 border-left-0 border-right-0 rounded-0" type="text" placeholder="Your listing name" name="title" required id="example-text-input">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-12 col-md-2 col-form-label">Tagline</label>
                                            <div class="col-12 col-md-10">
                                                <input class="form-control border-top-0 border-left-0 border-right-0 rounded-0" type="text" placeholder="Say something catchy!" name="tagline"  id="example-text-input">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-12 col-md-2 col-form-label">Price</label>
                                            <div class="col-12 col-md-10">
                                                <input class="form-control border-top-0 border-left-0 border-right-0 rounded-0" type="number" placeholder="Amount per day" name="price"  id="example-text-input">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-12 col-md-2 col-form-label">Description</label>
                                            <div class="col-12 col-md-10">
                                                <textarea class="form-control border-top-0 border-left-0 border-right-0 rounded-0" placeholder="Type listing description" name="description" required></textarea>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <hr/>

                                <div class="row p-4">
                                    <div class="col-12 col-sm-12 mb-3">
                                        <h6 class="lis-font-weight-500"><i class="fa fa-file-image-o pr-2 lis-f-14"></i> Images </h6>

                                    </div>
                                    <div class="col-12 col-sm-12">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-12 col-md-3 col-form-label">Cover Image</label>
                                            <div class="col-12 col-md-9">
                                                <input class="form-control border-0" type="file" name="image" required id="example-text-input">
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <hr/>

                                <div class="row p-4">
                                    <div class="col-12 col-sm-12 mb-3">
                                        <h6 class="lis-font-weight-500"><i class="fa fa-info-circle pr-2 lis-f-14"></i>  Contact information </h6>

                                    </div>
                                    <div class="col-12 col-sm-12">

                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-12 col-md-3 col-form-label">Phone number</label>
                                            <div class="col-12 col-md-9">
                                                <input class="form-control border-top-0 border-left-0 border-right-0 rounded-0" name="mobileNumber" required type="text" placeholder="Your phone number" id="example-text-input">
                                            </div>
                                        </div>

                                    </div>

                                </div>

                                <hr/>

                                <div class="row p-4">
                                    <div class="col-12 col-sm-12 mb-3">
                                        <h6 class="lis-font-weight-500"><i class="fa fa-map-o pr-2 lis-f-14"></i>  Location </h6>
                                    </div>
                                    <div class="col-12 col-sm-12">
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-12 col-md-2 col-form-label">Address</label>
                                            <div class="col-12 col-md-10">
                                                <input class="form-control border-top-0 border-left-0 border-right-0 rounded-0" placeholder="e.g. 2438 Progress Way, Donnelly, MN, 56235" type="text" name="address" required id="us2-address" >
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-12 col-md-2 col-form-label">Decimal degrees</label>
                                            <div class="col-12 col-md-5">
                                                <input class="form-control border-top-0 border-left-0 border-right-0 rounded-0" type="text" placeholder="Latitude" name="latitude" id="us2-lat">
                                            </div>
                                            <div class="col-12 col-md-5">
                                                <input class="form-control border-top-0 border-left-0 border-right-0 rounded-0" type="text" placeholder="Longitude" name="longitude" id="us2-lon">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-12 col-sm-12">
                                    <?php echo $errorMessage;?> 
                                        <input type="submit" class="btn btn-primary btn-default mt-3" id="submit-btn" name="submit" value="Save">
<!--                                         <a href="#" class="btn btn-primary btn-default mt-3"> Save & Preview</a>  -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            </form>
        </section>
        <!--End Pricing -->
        <!-- Features -->
        <section class="lis-bg-light">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-4 wow fadeInRight mb-4 mb-lg-0 text-center">
                        <div class="px-3">
                            <div class="h1 lis-dark"><i class="fa fa-money"></i></div>
                            <h6 class="lis-f-20 lis-font-weight-500">Money Back Guarantee</h6> We provide money back guarantee when you use the item below the time extension. </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4 wow fadeInRight mb-4 mb-lg-0 text-center">
                        <div class="px-3">
                            <div class="h1 lis-dark"><i class="fa fa-check-square-o"></i></div>
                            <h6 class="lis-f-20 lis-font-weight-500">Simple Add Listing</h6> We provide simple user interface to users to add list to user accounts </div>
                    </div>
                    <div class="col-12 col-md-6 col-lg-4 wow fadeInRight text-center">
                        <div class="px-3">
                            <div class="h1 lis-dark"><i class="fa fa-users"></i></div>
                            <h6 class="lis-f-20 lis-font-weight-500">Fast Support Team</h6> We fastly delivery your item and fastly takes the item from you when your given time expires. </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Features -->
        <!-- Footer-->
<section class="image-bg footer lis-grediant grediant-bt pb-0">
            <div class="background-image-maker"></div>
            <div class="holder-image"> <img src="dist/images/bg3.jpg" alt="" class="img-fluid d-none"> </div>
            <div class="container">
                <div class="row pb-5">
                    <div class="col-12 col-md-8">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                                <h5 class="footer-head">Useful Links</h5>
                                <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li>
                                        <A href="add-place-listing.php"><i class="fa fa-angle-right pr-1"></i> Add Listing</A>
                                    </li>
                                    <li>
                                        <A href="loginhead.php"><i class="fa fa-angle-right pr-1"></i> Sign In</A>
                                    </li>
                                    <li>
                                        <A href="signuphead.php"><i class="fa fa-angle-right pr-1"></i> Register</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Contact Us</A>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                                <h5 class="footer-head">My Account</h5>
                                <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li>
                                        <A href="user-profile.php"><i class="fa fa-angle-right pr-1"></i> Dashboard</A>
                                    </li>
                                    <li>
                                        <A href="user-profile.php#listing"><i class="fa fa-angle-right pr-1"></i> My Listing</A>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-md-0">
                                <!-- <h5 class="footer-head">Pages</h5> -->
                                <!-- <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Blog</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Our Partners</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> How It Work</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Privacy Policy</A>
                                    </li>
                                </ul> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="footer-logo">
                            <a href="#"><img src="dist/images/logo-v1.png" alt="" class="img-fluid" /></a>
                        </div>
                        <p class="my-4">Rental Express</p> <a href="#" class="text-white"></a>
                    </div>
                </div>
            </div>
        </section>
        <!--End  Footer-->
        <!-- Top To Bottom-->
        <a href="#" class="scrollup text-center lis-bg-primary lis-rounded-circle-50">
            <div class="text-white mb-0 lis-line-height-1_7 h3"><i class="icofont icofont-long-arrow-up"></i></div>
        </a>
        <!-- End Top To Bottom-->
        <!-- Login /Register Form-->
        <div class="container">


            <div id="modal" class="popupContainer" style="display: none;">
                <header class="popupHeader">
                    <span class="header_title">Login</span>
                    <span class="modal_close"><i class="fa fa-times"></i></span>
                </header>

                <div class="popupBody">
                    <!-- Social Login -->						

                    <!-- Username & Password Login form -->
                    <div class="user_login">
                        <form action="login.php" method="POST">

                            <input type="email" class="form-control border-top-0 border-left-0 border-right-0 rounded-0 " placeholder="Email address" name="email" />
                            <br />


                            <input type="password" class="form-control border-top-0 border-left-0 border-right-0 rounded-0 " placeholder="password" name="password" />
                            <br />

                            <div class="checkbox">
                                <input id="remember" type="checkbox" />
                                <label for="remember">Remember me on this computer</label>
                            </div>

                            <div class="action_btns">
                                <input class="btn btn-primary btn-default mt-3 w-100" type="submit" name="login-btn" value="Login">
<!--                                 <a href="#" class="btn btn-primary btn-default mt-3 w-100">Login</a> -->
                            </div>
                        </form>
                        <br/>
                        Sign in with your social network<br/>
                        <ul class="list-inline my-0">
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-facebook"></i></a></li>
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-twitter"></i></a></li>
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-linkedin"></i></a></li>
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-tumblr"></i></a></li>
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-pinterest-p"></i></a></li>
                        </ul>
                        <hr/>
                        Don't have an account <a href="#" class="register_form">Sign Up</a>
                    </div>

                    <!-- Register Form -->
                    <div class="user_register">
                        <form action="register.php" method="POST">

                            <input type="text" class="form-control border-top-0 border-left-0 border-right-0 rounded-0" placeholder="Username" name="userName" />
                            <br />


                            <input type="email" class="form-control border-top-0 border-left-0 border-right-0 rounded-0" placeholder="Email Address" name="email" />
                            <br />


                            <input type="password" class="form-control border-top-0 border-left-0 border-right-0 rounded-0" placeholder="Password" name="password" />
                            <br />


                            <input type="password" class="form-control border-top-0 border-left-0 border-right-0 rounded-0" placeholder="Re-enter Password" />
                            <br />

                            <div class="checkbox">
                                <input id="send_updates" type="checkbox" />
                                <label for="send_updates">Send me occasional email updates</label>
                            </div>

                            <div class="action_btns">
                                <input class="btn btn-primary btn-default mt-3 w-100" type="submit" name="register-btn" value="Register" >
<!--                                 <a href="#" class="btn btn-primary btn-default mt-3 w-100">Register</a> -->
                            </div>
                        </form>
                        <br/>
                        Register with your social network<br/>
                        <ul class="list-inline my-0">
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-facebook"></i></a></li>
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-twitter"></i></a></li>
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-linkedin"></i></a></li>
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-tumblr"></i></a></li>
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-pinterest-p"></i></a></li>
                        </ul>
                        <hr/>
                        Already have an account <a href="#" class="login_form">Sign In</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Login /Register Form-->
        <!-- jQuery -->

        <script src="../../../../../../external.html?link=https://maps.googleapis.com/maps/api/js?key=AIzaSyAjsIpyXFoB7_5_E2DgO_-CTDMHKM0cW4I&amp;sensor=false&amp;libraries=places"></script>
        <script src="dist/js/plugins.min.js"></script>
        <script src="dist/js/common.js"></script>
        <script src="dist/js/jquery.locationpicker.js"></script>
        <script src="dist/js/add-listing.js"></script>
    </body>


<!-- Mirrored from psd2allconversion.com/templates/themeforest/html/lister/default/add-place-listing.php by HTTrack Website Copier/3.x [XR&CO'2017], Sat, 20 Jan 2018 16:04:03 GMT -->
</html>