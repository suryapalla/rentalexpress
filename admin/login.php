<?php
	session_start();
	if (isset($_SESSION['adminDetails'])) {
		session_destroy();
	}
	if (isset($_SESSION['message'])) {
		$i = 1;
		$message = $_SESSION['message'];
		session_unset('message');
	}
	else{
		$i = 0;
		$message = '';
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Admin</title>
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link id="bootstrap-style" href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
	<!-- end: CSS -->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->
	
			<style type="text/css">
			body { background: url(img/bg-login.jpg) !important; }
		</style>
		
		
		
</head>

<body>
		<div class="container-fluid-full">
		<div class="row-fluid">
					
			<div class="row-fluid">
				<div class="login-box">
					<button style="display: none;" class="btn btn-primary noty" data-noty-options='{"text":"<?php echo $message; ?>","layout":"topRight","type":"error"}'><i class="halflings-icon white bell"></i> Top Left</button>
					<h2>Login to your account</h2>
					<form class="form-horizontal" action="validate.php" method="post">
						<fieldset>
							
							<div class="input-prepend" title="Username">
								<span class="add-on"><i class="halflings-icon user"></i></span>
								<input name="userName" required class="input-large span10" name="userName" id="userName" type="text" placeholder="type username"/>
							</div>
							<div class="clearfix"></div>

							<div class="input-prepend" title="Password">
								<span class="add-on"><i class="halflings-icon lock"></i></span>
								<input name="password" required class="input-large span10" name="password" id="password" type="password" placeholder="type password"/>
							</div>
							<div class="clearfix"></div>
							

							<div class="button-login">	
								<button type="submit" id="login-btn" class="btn btn-primary">Login</button>
							</div>
							<div class="clearfix"></div>
					</form>	
				</div><!--/span-->
			</div><!--/row-->
			

	</div><!--/.fluid-container-->
	
		</div><!--/fluid-row-->
	
	<!-- start: JavaScript-->

		<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-migrate-1.0.0.min.js"></script>
	
		<script src="js/jquery-ui-1.10.0.custom.min.js"></script>
	
		<script src="js/jquery.ui.touch-punch.js"></script>
	
		<script src="js/modernizr.js"></script>
	
		<script src="js/bootstrap.min.js"></script>
	
		<script src="js/jquery.cookie.js"></script>
	
		<script src='js/fullcalendar.min.js'></script>
	
		<script src='js/jquery.dataTables.min.js'></script>

		<script src="js/excanvas.js"></script>
	<script src="js/jquery.flot.js"></script>
	<script src="js/jquery.flot.pie.js"></script>
	<script src="js/jquery.flot.stack.js"></script>
	<script src="js/jquery.flot.resize.min.js"></script>
	
		<script src="js/jquery.chosen.min.js"></script>
	
		<script src="js/jquery.uniform.min.js"></script>
		
		<script src="js/jquery.cleditor.min.js"></script>
	
		<script src="js/jquery.noty.js"></script>
	
		<script src="js/jquery.elfinder.min.js"></script>
	
		<script src="js/jquery.raty.min.js"></script>
	
		<script src="js/jquery.iphone.toggle.js"></script>
	
		<script src="js/jquery.uploadify-3.1.min.js"></script>
	
		<script src="js/jquery.gritter.min.js"></script>
	
		<script src="js/jquery.imagesloaded.js"></script>
	
		<script src="js/jquery.masonry.min.js"></script>
	
		<script src="js/jquery.knob.modified.js"></script>
	
		<script src="js/jquery.sparkline.min.js"></script>
	
		<script src="js/counter.js"></script>
	
		<script src="js/retina.js"></script>

		<script src="js/custom.js"></script>
	<!-- end: JavaScript-->
	<!-- <script type="text/javascript">
		$(document).ready(function() {
			$("#login-btn").click(function() {
				$("#password").attr("style","border:1px solid #eee !important");
				$("#login-btn").attr("type","button");
				var userName = $("#userName").val();
				var password = $("#password").val();
				var popUp = '"text":"You Entered invalid credentials","layout":"topRight","type":"error"';
				$.ajax({
					url : "loginValidate.php",
					type : "POST",
					async : false,
					data : {
						"userName" : userName,
						"password" : password,
						"done" : 1
					},
					success : function(data) {
						if (data =='success') {
							$("#login-btn").attr("type","submit");
						}
						else{
							var notificationText = '{'+popUp+'}';
							$("#password").attr("style","border:1px solid #ff0000 !important");
							$(".noty").attr("data-noty-options",notificationText);
							$(".noty").trigger('click');
						}
					}
				})
			})
		});
	</script> -->
	<?php
		if ($i==1) {
	?>
		<script type="text/javascript">
			$(document).ready(function() {
				$(".noty").trigger('click');
			});
		</script>
	<?php
		}
	?>
</body>
</html>
