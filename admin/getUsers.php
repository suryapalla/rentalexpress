<?php
	function getUsers($con)
	{		
		require 'db.php';
		$sql = 'SELECT * FROM users';
		$result = $con->query($sql);
		return $result;
	}
	function getBookings($con)
	{		
		require 'db.php';
		$sql = 'SELECT * FROM bookings';
		$result = $con->query($sql);
		return $result;
	}
?>