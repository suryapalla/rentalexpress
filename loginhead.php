<?php
    session_start();

    require 'db.php';
    require 'Category.php';
    $cat = new Category;
    $list = $cat->getList($con);
    $message='';
    $errorMessage='';
    if (isset($_SESSION['message'])) {
        $errorMessage = $_SESSION['message'];
        $_SESSION['message']='';
    }
    if (isset($_SESSION['userDetails'])) {
        $message='Success';
    }
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Rental Express</title>
        <link rel="shortcut icon" href="dist/images/favicon.ico">
        <!--Plugin CSS-->
        <link href="dist/css/plugins.min.css" rel="stylesheet">
        <!--main Css-->
        <link href="dist/css/main.min.css" rel="stylesheet"> 
        <style type="text/css">
            #submit-btn{
                cursor: pointer;
            }
        </style>
</head>
<body>

        <div id="header-fix" class="header fixed-top">
            <nav class="navbar navbar-toggleable-md navbar-expand-lg navbar-light py-lg-0 py-4">
                <a class="navbar-brand mr-4 mr-md-5" href="index.php"><img src="dist/images/logo-v1.png" alt=""></a>
                <div id="dl-menu" class="dl-menuwrapper d-block d-lg-none float-right">
                    <button>Open Menu</button>
                    <ul class="dl-menu">

                        <li class="nav-item active">
                            <a class="nav-link" href="index.php" aria-expanded="false">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="listing-categories-style2.php" aria-expanded="false">Explore</a>
                        </li>
                        
                        <?php
                            if ($message=='Success') {
                        ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Pages</a>
                            <ul class="dl-submenu">
                                <li class="dl-back"><a href="#">back</a></li>
                            <li><a href="user-profile.php"> User Profile</a></li>
                            <li><a href="log-out.php" class="fa fa-sign-out">Log-out</a></li>
                            </ul>
                        </li>

                        <?php
                            }
                        ?>
                            

                <?php
                    if ($message!='Success') {
                ?>

                    <ul class="list-unstyled my-2 my-lg-0">
                        <li>
                             <a href="loginhead.php" class="text-white"><i class="fa fa-lock pr-2"></i> Sign In</a>
                        </li>
                    </ul>
                     
                <?php
                    }
                else{
                ?> 

                    <ul class="list-unstyled my-2 my-lg-0">
                        <li>
                             <a href="user-profile.php" class="text-white"><i class="fa fa-user-circle-o pr-2"></i>Welcome <?php echo$_SESSION['userDetails']['userName'];?></a>
                        </li>
                    </ul>
                        <li> <a href="add-place-listing.php" ><i class="fa fa-plus pr-1"></i> Add Listing</a></li>
                <?php
                    }
                ?>





                    </ul>
                </div>

                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item"> <a class="nav-link" href="index.php" aria-expanded="false">Home</a>
                        <li class="nav-item active"> <a class="nav-link" href="listing-categories-style2.php" aria-expanded="false">Explore</a>
                        </li>
                        
                        <?php
                            if ($message=='Success') {
                        ?>
                        <li class="nav-item dropdown"> <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Pages <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                            <li><a href="user-profile.php"> User Profile</a></li>
                            <li><a href="log-out.php" class="fa fa-sign-out">Log-out</a></li>
                            </ul>
                        </li>
                        <?php
                            }
                        ?>
                            
                    </ul>
                <?php
                    if ($message!='Success') {
                ?>

                    <ul class="list-unstyled my-2 my-lg-0">
                        <li>
                             <a href="signuphead.php" class="text-white"><i class="fa fa-lock pr-2"></i> Register</a>
                        </li>
                    </ul>
                     
                <?php
                    }
                else{
                ?> 

                    <ul class="list-unstyled my-2 my-lg-0">
                        <li>
                             <a href="user-profile.php" class="text-white"><i class="fa fa-user-circle-o pr-2"></i>Welcome <?php echo$_SESSION['userDetails']['userName'];?></a>
                        </li>
                    </ul>
                                        <a href="add-place-listing.php" class="btn btn-outline-light btn-sm ml-0 ml-lg-4 mt-3 mt-lg-0"><i class="fa fa-plus pr-1"></i> Add Listing</a> </div>
                <?php
                    }
                ?>
            </nav>
        </div>
        <!--End header -->
        <div id="modal" class="popupContainer" style="display: block; opacity: 1; z-index: 1020; left: 50%; margin-left: -165px; top: 120px;">
                <header class="popupHeader">
                    <span class="header_title">Login</span>
                    <span class="modal_close"><i class="fa fa-times"></i></span>
                </header>

                <div class="popupBody">
                    <!-- Social Login -->						

                    <!-- Username & Password Login form -->
                    <div style="display: block;">
                        <form action="login.php" method="POST">

                            <input type="email" class="form-control border-top-0 border-left-0 border-right-0 rounded-0 " required placeholder="Email address" name="email">
                            <br>


                            <input type="password" class="form-control border-top-0 border-left-0 border-right-0 rounded-0 " required placeholder="password" name="password">
                            <br>

                            <div class="checkbox">
                             <?php echo $errorMessage;?>
                            </div>

                            <div class="action_btns">
                                <input id="submit-btn" class="btn btn-primary btn-default mt-3 w-100" type="submit" name="login-btn" value="Login">
<!--                                 <a href="#" class="btn btn-primary btn-default mt-3 w-100">Login</a> -->
                            </div>
                        </form>
                        <br>
                        <hr>
                        Don't have an account <a href="signuphead.php" >Sign Up</a>
                    </div>


                </div>
            </div>
         <!--popup ended-->
<div style="height: 700px;"></div>
        <!-- Footer-->
<section class="image-bg footer lis-grediant grediant-bt pb-0">
            <div class="background-image-maker"></div>
            <div class="holder-image"> <img src="dist/images/bg3.jpg" alt="" class="img-fluid d-none"> </div>
            <div class="container">
                <div class="row pb-5">
                    <div class="col-12 col-md-8">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                                <h5 class="footer-head">Useful Links</h5>
                                <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li>
                                        <A href="add-place-listing.php"><i class="fa fa-angle-right pr-1"></i> Add Listing</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Contact Us</A>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                                <h5 class="footer-head">My Account</h5>
                                <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li>
                                        <A href="user-profile.php"><i class="fa fa-angle-right pr-1"></i> Dashboard</A>
                                    </li>
                                    <li>
                                        <A href="user-profile.php#listing"><i class="fa fa-angle-right pr-1"></i> My Listing</A>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-md-0">
                                <!-- <h5 class="footer-head">Pages</h5> -->
                                <!-- <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Blog</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Our Partners</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> How It Work</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Privacy Policy</A>
                                    </li>
                                </ul> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="footer-logo">
                            <a href="#"><img src="dist/images/logo-v1.png" alt="" class="img-fluid" /></a>
                        </div>
                        <p class="my-4">Rental Express</p> <a href="#" class="text-white"></a>
                    </div>
                </div>
            </div>
        </section>
        <!--End  Footer-->
        <!-- Top To Bottom-->
        <a href="#" class="scrollup text-center lis-bg-primary lis-rounded-circle-50">
            <div class="text-white mb-0 lis-line-height-1_7 h3"><i class="icofont icofont-long-arrow-up"></i></div>
        </a>
        <!-- End Top To Bottom-->

        <!-- jQuery -->
        <script src="dist/js/plugins.min.js"></script>
        <script src="dist/js/common.js"></script>

</body>
</html>