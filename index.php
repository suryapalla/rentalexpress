<?php
    session_start();

    require 'db.php';
    require 'Category.php';
    $cat = new Category;
    $list = $cat->getList($con);
    $catList = $cat->getList($con);
    $searchList = $cat->searchItems($con);
    $message='';
    $errorMessage='';
    if (isset($_SESSION['message'])) {
        $errorMessage = $_SESSION['message'];
        $_SESSION['message']='';
    }
    if (isset($_SESSION['userDetails'])) {
        $message='Success';
    }
?>
<!DOCTYPE html>
<html lang="en">


<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Rental Express</title>
        <link rel="shortcut icon" href="dist/images/favicon.ico">
        <!--Plugin CSS-->
        <link href="dist/css/plugins.min.css" rel="stylesheet">
        <!--main Css-->
        <link href="dist/css/main.min.css" rel="stylesheet"> 
        <link rel="stylesheet" type="text/css" href="pre/css/style.css">
        <style type="text/css">
            #submit-btn{
                cursor: pointer;
            }
        </style>
    </head>

    <body id="body">
        <div class="loading" id="pre-loader"> 
          <div class="finger finger-1">
            <div class="finger-item">
              <span></span><i></i>
            </div>
          </div>
                    <div class="finger finger-2">
            <div class="finger-item">
              <span></span><i></i>
            </div>
          </div>
                    <div class="finger finger-3">
            <div class="finger-item">
              <span></span><i></i>
            </div>
          </div>
                    <div class="finger finger-4">
            <div class="finger-item">
              <span></span><i></i>
            </div>
          </div>
                    <div class="last-finger">
            <div class="last-finger-item"><i></i></div>
          </div>
        </div>
        <div id="main-page" style="display: none;">
            <!-- header -->
            <div id="header-fix" class="header fixed-top transperant">
                <nav class="navbar navbar-toggleable-md navbar-expand-lg navbar-light py-lg-0 py-4">
                    <a class="navbar-brand mr-4 mr-md-5" href="index-2.html"><img src="dist/images/logo-v1.png" alt=""></a>
                    <div id="dl-menu" class="dl-menuwrapper d-block d-lg-none float-right">
                        <button>Open Menu</button>
                        <ul class="dl-menu">

                            <li class="nav-item active dropdown">
                                <a class="nav-link" href="index.php">Home</a>

                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="listing-categories-style2.php">Explore</a>
                            </li>
                           <?php
                                if ($message=='Success') {
                            ?>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Pages</a>
                                <ul class="dl-submenu">
                                    <li class="dl-back"><a href="#">back</a></li>
                                <li><a href="user-profile.php"> User Profile</a></li>
                                <li><a href="log-out.php" class="fa fa-sign-out">Log-out</a></li>
                                </ul>
                            </li>
                            <?php
                                }
                            ?>
                                


                    <?php
                        if ($message!='Success') {
                    ?>

                        <ul class="list-unstyled my-2 my-lg-0">
                            <li>
                                 <a href="loginhead.php" class="text-white"><i class="fa fa-lock pr-2"></i> Sign In</a>
                            </li>
                        </ul>
                         
                    <?php
                        }
                    else{
                    ?> 

                        <ul class="list-unstyled my-2 my-lg-0">
                            <li>
                                 <a href="user-profile.php" class="text-white"><i class="fa fa-user-circle-o pr-2"></i>Welcome <?php echo$_SESSION['userDetails']['userName'];?></a>
                            </li>
                        </ul>
                            <li> <a href="add-place-listing.php" ><i class="fa fa-plus pr-1"></i> Add Listing</a></li>
                    <?php
                        }
                    ?>





                        </ul>
                    </div>

                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active dropdown"> <a class="nav-link" href="index.php">Home</a>
                            </li>
                            <li class="nav-item dropdown"> <a class="nav-link" href="listing-categories-style2.php" >Explore</a>
                            </li>
                            <?php
                                if ($message=='Success') {
                            ?>
                            <li class="nav-item dropdown"> <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Pages <i class="fa fa-angle-down"></i></a>
                                <ul class="dropdown-menu">
                                    <li><a href="user-profile.php"> User Profile</a></li>
                                    <li><a href="log-out.php" class="fa fa-sign-out">Log-out</a></li>
                                    </ul>
                            </li>
                            <?php
                                }
                            ?>
                                
                        </ul>
                    <?php
                        if ($message!='Success') {
                    ?>

                        <ul class="list-unstyled my-2 my-lg-0">
                            <li>
                                 <a href="loginhead.php" class="text-white"><i class="fa fa-lock pr-2"></i> Sign In</a>
                            </li>
                        </ul>
                         
                    <?php
                        }
                    else{
                    ?> 

                        <ul class="list-unstyled my-2 my-lg-0">
                            <li>
                                 <a href="user-profile.php" class="text-white"><i class="fa fa-user-circle-o pr-2"></i>Welcome <?php echo$_SESSION['userDetails']['userName'];?></a>
                            </li>
                        </ul>
                                            <a href="add-place-listing.php" class="btn btn-outline-light btn-sm ml-0 ml-lg-4 mt-3 mt-lg-0"><i class="fa fa-plus pr-1"></i> Add Listing</a> </div>
                    <?php
                        }
                    ?>


                </nav>
            </div>
            <!--End header -->
            <!-- Page Inner -->
            <section class="image-bg lis-grediant grediant-tb">
                <div class="background-image-maker"></div>
                <div class="holder-image"> <img src="dist/images/bg4.jpg" alt="" class="img-fluid d-none"> </div>
                <div class="container">
                    <div class="row justify-content-center pt-5">
                        <div class="col-12 col-md-10 text-center wow fadeInUp">
                            <div class="heading pb-5">
                                <h1 class="display-4">Search your Need</h1>
                                <h4 class="font-weight-normal mb-0">Expolore the things like vechicles,homes and more</h4> </div>
                            <ul class="nav nav-tabs flex-column flex-sm-row" id="myTab" role="tablist">
                                <li class="nav-item mr-md-1"> <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"><i class="fa fa-map-marker pr-1"></i> Places</a> </li>
                            </ul>
                            <div class="tab-content bg-white p-5 rounded-bottom rounded-right" id="myTabContent">
                            <form method="GET" action="listing-no-map-fullwidth.php">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    <div class="row">

                                        <div class="col-12 col-md-12">
                                            <div class="form-group">
                                                <input list="items" class="form-control border-top-0 border-left-0 border-right-0 rounded-0 pl-4" placeholder="What are you looking for?" required name="item" />
                                                <datalist id="items" >
                <?php
                    while ($itemsList = mysqli_fetch_array($searchList)) 
                    {
                        ?>
                                                    <option value="<?php echo $itemsList['title']; ?>"><?php echo $itemsList['category']; ?></option>
                        <?php
                    }
                ?>                
                                                </datalist>
                                                <div class="lis-search"> <i class="fa fa-search lis-primary"></i> </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-4">
                                            <div class="form-group">
                                                <select name="category" class="form-control border-top-0 border-left-0 border-right-0 rounded-0 pl-4">
                                                    <option> All Categories</option>
                <?php
                    while ($names = mysqli_fetch_array($catList) ) {
                ?>
                                                    <option><?php echo $names['name'];?></option>
                <?php
                    }
                ?>
                                                </select>
                                                <div class="lis-search"> <i class="fa fa-tags lis-primary"></i> </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-4">
                                         <input class="btn btn-primary btn-block btn-lg" type="submit" value="Search">
                                        </div>
                                    </div>
                                </div>
                            </form>
                                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                    <div class="row">
                                        <div class="col-12 col-md-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control border-top-0 border-left-0 border-right-0 rounded-0 pl-4" placeholder="What are you looking for?" />
                                                <div class="lis-search"> <i class="fa fa-search lis-primary"></i> </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-4">
                                            <div class="form-group">
                                                <input type="text" class="form-control border-top-0 border-left-0 border-right-0 rounded-0 pl-4" placeholder="Location" />
                                                <div class="lis-search"> <i class="fa fa-map-o lis-primary"></i> </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-4">
                                            <div class="form-group">
                                                <select class="form-control border-top-0 border-left-0 border-right-0 rounded-0 pl-4">
                                                    <option> All Categories</option>
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                </select>
                                                <div class="lis-search"> <i class="fa fa-tags lis-primary"></i> </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-4"> <a href="#" class="btn btn-primary btn-block btn-lg"><i class="fa fa-search pr-1"></i> Search</a> </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                    <div class="row">
                                        <div class="col-12 col-md-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control border-top-0 border-left-0 border-right-0 rounded-0 pl-4" placeholder="What are you looking for?" />
                                                <div class="lis-search"> <i class="fa fa-search lis-primary"></i> </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6 col-lg-4">
                                            <div class="form-group">
                                                <input type="text" class="form-control border-top-0 border-left-0 border-right-0 rounded-0 pl-4" placeholder="Location" />
                                                <div class="lis-search"> <i class="fa fa-map-o lis-primary"></i> </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-lg-4"> <a href="#" class="btn btn-primary btn-block btn-lg"><i class="fa fa-search pr-1"></i> Search</a> </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--End Page Inner -->
            <!-- Categories -->
            <section class="lis-grediant grediant-tb-white">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-10 text-center">
                            <div class="heading pb-4">
                                <h5 class="lis-light">Find the best</h5>
                                <h2 class="f-weight-500">Discover Our Featured Categories.</h2> </div>
                        </div>
                    </div>
                </div>
                <div class="container-fluid px-0">
                    <div class="fullwidth-carousel-container center2 slider">
            <?php
                $i=1;
                while ($row=mysqli_fetch_array($list)) {
                    if ($i<7) {
                       
            ?>
                        <div>
                            <div class="card lis-brd-light text-center text-lg-left lis-info lis-relative">
                                <a href="<?php echo "listing-no-map-fullwidth.php?category=".$row['name']?>">
                                    <div class="lis-grediant grediant-tb-light lis-relative modImage rounded">
                                        <img src="<?php echo $row['imagePath'];?>" alt=<?php echo $row['name'];?> class="img-fluid rounded" />
                                    </div>
                                    <div class="lis-absolute lis-left-20 lis-top-20 lis-bg4 lis-icon lis-rounded-circle-50 text-center">
                                        <div class="text-white mb-0 lis-line-height-2_5 h4">
                                            <i class="icofont <?php echo $row['icon'];?>" ></i>
                                        </div>
                                    </div>
                                </a>
                                <div class="hover-text lis-absolute lis-left-20 lis-bottom-20 lis-font-roboto text-white text-left">
                                    <h6 class="text-white mb-0"><?php echo $row['name'];?></h6> <span class="lis-font-roboto"><?php echo $row['listingCount'];?> listing</span> </div>
                            </div>
                        </div>
            <?php
                    }
                    $i++;
                }
            ?>
                    </div>
                </div>
                <div class="container">
                    <div class="row justify-content-center mt-5">
                        <div class="col-12 text-center"> <a href="listing-categories-style2.php" class="btn btn-success btn-default">View More Featured Categories</a> </div>
                    </div>
                </div>
            </section>
            <!--End Categories -->
            <!-- Work -->
            <section>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-10 text-center">
                            <div class="heading pb-4">
                                <h2>How Does It Work</h2>
                                <h5 class="font-weight-normal lis-light">Discover & connect with top-rated local businesses</h5> </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-4 text-center wow fadeInUp mb-5 mb-md-0">
                            <div class="icon-box box-line box-line-dotted1 lis-relative"> <img src="dist/images/icon-1.png" alt="" class="img-fluid mb-4 z-index-99  lis-relative" />
                                <h5>1. Find Interesting Place</h5>
                                <p>Find your interesting place and interesting item, and add To your location. </p>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 text-center wow fadeInUp mb-5 mb-md-0">
                            <div class="icon-box box-line box-line-dotted2 lis-relative"> <img src="dist/images/icon-2.png" alt="" class="img-fluid mb-4 z-index-99  lis-relative" />
                                <h5>2. Choose a Category</h5>
                                <p>Choose the item in the given categoery and add to your list.</p>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 text-center wow fadeInUp mb-5 mb-md-0">
                            <div class="icon-box"> <img src="dist/images/icon-3.png" alt="" class="img-fluid mb-4 z-index-99  lis-relative" />
                                <h5>3. Contact with Us</h5>
                                <p>Contact with us because the item's owner needs security. So, We deletes the communication with owners.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--End Work -->

            <!-- Footer-->
<section class="image-bg footer lis-grediant grediant-bt pb-0">
            <div class="background-image-maker"></div>
            <div class="holder-image"> <img src="dist/images/bg3.jpg" alt="" class="img-fluid d-none"> </div>
            <div class="container">
                <div class="row pb-5">
                    <div class="col-12 col-md-8">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                                <h5 class="footer-head">Useful Links</h5>
                                <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li>
                                        <A href="add-place-listing.php"><i class="fa fa-angle-right pr-1"></i> Add Listing</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Contact Us</A>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                                <h5 class="footer-head">My Account</h5>
                                <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li>
                                        <A href="user-profile.php"><i class="fa fa-angle-right pr-1"></i> Dashboard</A>
                                    </li>
                                    <li>
                                        <A href="user-profile.php#listing"><i class="fa fa-angle-right pr-1"></i> My Listing</A>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-md-0">
                                <!-- <h5 class="footer-head">Pages</h5> -->
                                <!-- <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Blog</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Our Partners</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> How It Work</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Privacy Policy</A>
                                    </li>
                                </ul> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="footer-logo">
                            <a href="#"><img src="dist/images/logo-v1.png" alt="" class="img-fluid" /></a>
                        </div>
                        <p class="my-4">Rental Express</p> <a href="#" class="text-white"></a>
                    </div>
                </div>
            </div>
        </section>
            <!--End  Footer-->
            <!-- Top To Bottom-->
            <a href="#" class="scrollup text-center lis-bg-primary lis-rounded-circle-50">
                <div class="text-white mb-0 lis-line-height-1_7 h3"><i class="icofont icofont-long-arrow-up"></i></div>
            </a>
            <!-- End Top To Bottom -->
        </div>
        <!-- jQuery -->
        <script src="dist/js/plugins.min.js"></script>
        <script src="dist/js/common.js"></script>
        <script type="text/javascript">
            window.onload = setTimeout(function(){
                document.getElementById('pre-loader').style.display = "none";
                document.getElementById('main-page').style.display = "block";
                document.getElementById('body').style.backgroundColor = "transparent";
            },3000);
        </script>
    </body>


</html>