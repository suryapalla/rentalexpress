<?php
    session_start();

    require 'db.php';
    require 'Category.php';
    $cat = new Category;
    $list = $cat->getRentersById($con,$_SESSION['userDetails']['userId']);
    $list2 = $cat->getReply($con,$_SESSION['userDetails']['userId']);
    $message='';
    $errorMessage='';
    if (isset($_SESSION['messageACK'])) {
        $messageACK = $_SESSION['messageACK'];
        $_SESSION['messageACK']='';
    }
    else{
        $messageACK = '';
    }
    if (isset($_SESSION['userDetails'])) {
        $message='Success';
    }
    else{
        header("location:loginhead.php");
    }
?>
<!DOCTYPE html>
<html lang="en">


<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Rental Express</title>
        <link rel="shortcut icon" href="dist/images/favicon.ico">
        <!--Plugin CSS-->
        <link href="dist/css/plugins.min.css" rel="stylesheet">
        <!--main Css-->
        <link href="dist/css/main.min.css" rel="stylesheet"> 
    </head>

    <body>
        <!-- header -->
        <div id="header-fix" class="header fixed-top transperant">
            <nav class="navbar navbar-toggleable-md navbar-expand-lg navbar-light py-lg-0 py-4">
                <a class="navbar-brand mr-4 mr-md-5" href="index.php"><img src="dist/images/logo-v1.png" alt=""></a>
                <div id="dl-menu" class="dl-menuwrapper d-block d-lg-none float-right">
                    <button>Open Menu</button>
                    <ul class="dl-menu">

                        <li class="nav-item">
                            <a class="nav-link" href="index.php" aria-expanded="false">Home</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Explore</a>
                        </li>
                       <?php
                            if ($message=='Success') {
                        ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Pages</a>
                            <ul class="dl-submenu">
                                <li class="dl-back"><a href="#">back</a></li>
                            <li><a href="user-profile.php"> User Profile</a></li>
                            <li><a href="log-out.php" class="fa fa-sign-out">Log-out</a></li>
                            </ul>
                        </li>
                        <?php
                            }
                        ?>
                            


                <?php
                    if ($message!='Success') {
                ?>

                    <ul class="list-unstyled my-2 my-lg-0">
                        <li>
                             <a href="#modal" class="text-white login_form"><i class="fa fa-lock pr-2"></i> Sign In | Register</a>
                        </li>
                    </ul>
                     
                <?php
                    }
                else{
                ?> 

                    <ul class="list-unstyled my-2 my-lg-0">
                        <li>
                             <a href="user-profile.php" class="text-white"><i class="fa fa-user-circle-o pr-2"></i>Welcome <?php echo$_SESSION['userDetails']['userName'];?></a>
                        </li>
                    </ul>

                <?php
                    }
                ?>

                        <li> <a href="add-place-listing.php" ><i class="fa fa-plus pr-1"></i> Add Listing</a></li>



                    </ul>
                </div>

                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item"> <a class="nav-link" href="index.php"  aria-expanded="false">Home</a>
                        </li>
                        <li class="nav-item"> <a class="nav-link" href="listing-categories-style2.php" aria-expanded="false">Explore</a>
                        </li>
                       <?php
                            if ($message=='Success') {
                        ?>
                        <li class="nav-item dropdown"> <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Pages <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                            <li><a href="user-profile.php"> User Profile</a></li>
                            <li><a href="log-out.php" class="fa fa-sign-out">Log-out</a></li>
                            </ul>
                        </li>
                        <?php
                            }
                        ?>                            
                    </ul>
                <?php
                    if ($message!='Success') {
                ?>

                    <ul class="list-unstyled my-2 my-lg-0">
                        <li>
                             <a href="#modal" class="text-white login_form"><i class="fa fa-lock pr-2"></i> Sign In</a>
                        </li>
                    </ul>
                     
                <?php
                    }
                else{
                ?> 

                    <ul class="list-unstyled my-2 my-lg-0">
                        <li>
                             <a href="user-profile.php" class="text-white"><i class="fa fa-user-circle-o pr-2"></i>Welcome <?php echo$_SESSION['userDetails']['userName'];?></a>
                        </li>
                    </ul>

                <?php
                    }
                ?> 
                    <a href="add-place-listing.php" class="btn btn-outline-light btn-sm ml-0 ml-lg-4 mt-3 mt-lg-0"><i class="fa fa-plus pr-1"></i> Add Listing</a> </div>
            </nav>
        </div>
        <!--End header -->
        <!-- Profile Cover -->
        <section class="image-bg lis-grediant grediant-bt-dark text-white pb-4 profile-inner">
            <div class="background-image-maker"></div>
            <div class="holder-image"> <img src="dist/images/bg7.jpg" alt="" class="img-fluid d-none"> </div>
            <div class="container">
                <div class="row justify-content-center wow fadeInUp">
                    <div class="col-12 col-md-12 mb-4 mb-lg-0">
                        <a href="#" class="text-white">
                            <div class="media d-block d-sm-flex text-sm-left text-center"> <img src="dist/images/profile-4.png" class="img-fluid d-sm-flex mr-0 mr-sm-4 border border-white lis-border-width-4 rounded mb-4 mb-md-0" alt="" />
                                <div class="media-body align-self-center">
                                    <h5 class="text-white lis-font-weight-500 lis-line-height-1"><?php echo $_SESSION['userDetails']['userName']; ?></h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>
        <!--End Profile Cover -->
        <!-- Profile header -->
        <div class="profile-header">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-xl-9 order-xl-1 order-2 text-xl-right text-center">
                        <ul class="nav nav-pills flex-column flex-sm-row lis-font-poppins" id="myTab" role="tablist">
                            <li class="nav-item ml-0"> <a class="nav-link lis-light py-4 lis-relative mr-3 active" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-expanded="true"> Profile</a> </li>
                            <li class="nav-item listing-link ml-0"> <a class="nav-link lis-light py-4 lis-relative mr-3" data-toggle="tab" href="#listing" role="tab" aria-controls="listing"> My Listings</a> </li>
                            <li class="nav-item ml-0"> <a class="nav-link lis-light py-4 lis-relative mr-3" data-toggle="tab" href="#reviews" role="tab" aria-controls="reviews">Messages </a> </li>
                        </ul>
                    </div>
                    <div class="col-12 col-xl-3 align-self-center order-xl-2 order-1 text-xl-right text-center mt-4 mt-xl-0">
                        <a href="add-place-listing.php" class="btn btn-primary btn-default"> <i class="fa fa-plus pr-1"></i> Add Listing</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- End header -->
        <!-- Profile Content -->
        <section class="lis-bg-light pt-5">
            <div class="container">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile">
                        <div class="row">
                            <div class="col-12 col-lg-8 mb-5 mb-lg-0">
                                <h6 class="lis-font-weight-500"><i class="fa fa-user-circle-o pr-2 lis-f-14"></i> Basic Info</h6>
                                <div class="card lis-brd-light wow fadeInUp mb-4">
                                    <div class="card-body p-4">
                                        <div class="media d-md-flex d-block text-center text-md-left">
                                            <a href="#"><img src="dist/images/user1.jpg" class="img-fluid d-md-flex mr-0 mr-md-5 rounded " alt="" width="280"></a>
                                            <div class="media-body align-self-center mt-4 mt-md-0">
                                                <h5 class="mb-0 lis-font-weight-500"><a href="#" class="lis-dark"><?php echo $_SESSION['userDetails']['userName']; ?></a></h5>
                                                <dl class="row my-2 lis-line-height-2"> <dt class="col-xl-3 col-md-4 lis-font-weight-500 lis-dark">User Name:</dt>
                                                    <dd class="col-xl-9 col-md-8"><?php echo $_SESSION['userDetails']['userName']; ?></dd> <dt class="col-xl-3 col-md-4 lis-font-weight-500 lis-dark">Email:</dt>
                                                    <dd class="col-xl-9 col-md-8"><?php echo $_SESSION['userDetails']['email']; ?></dd>
                                                </dl>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h6 class="lis-font-weight-500"><i class="fa fa-envelope pr-2 lis-f-14"></i>  Send Message</h6>
                                <div class="card lis-brd-light wow fadeInUp">
                                    <div class="card-body p-4">
                                    <form action="sendit.php" method="POST">
                                        <div class="form-group lis-relative">
                                            <textarea name="userMessage" class="form-control border-top-0 border-left-0 border-right-0 rounded-0 pl-4" placeholder="Message"></textarea>
                                            <div class="lis-search"> <i class="fa fa-pencil lis-primary lis-left-0 lis-top-10"></i> </div>
                                        </div>
                                            <input type="submit" class="btn btn-primary btn-default" name="submit-btn" value="Submit Message" >
                                            <?php echo $messageACK; ?>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="listing" role="tabpanel" aria-labelledby="listing">
                        <div class="row portfolio-box2">
        <?php
            while ($row = mysqli_fetch_array($list)){
        ?>
                            <div class="post col-12 col-md-6 col-lg-6 col-xl-4 wow fadeInUp mb-4">
                                <div class="card lis-brd-light text-center text-lg-left">
                                    <a href="#">
                                        <div class="lis-grediant grediant-tb-light2 lis-relative modImage rounded-top"> <img src="<?php echo $row['imagePath']; ?>" alt="" class="img-fluid rounded-top w-100" /> </div>
                                        <div class="lis-absolute lis-right-20 lis-top-20">
                                            <div class="lis-post-meta border border-white text-white rounded lis-f-14">Open</div>
                                        </div>
                                    </a>
                                    <div class="card-body pt-0">
                                        <div class="media d-block d-lg-flex lis-relative">
                                            <div class="media-body align-self-start mt-2">
                                                <h6 class="mb-0 lis-font-weight-600"><A href="#" class="lis-dark">
                                                    <?php echo $row['title']; ?>
                                                </A></h6> </div>
                                        </div>
                                        <ul class="list-unstyled my-4 lis-line-height-2">
                                            <li><i class="fa fa-map-o pr-2"></i> <?php echo $row['address']; ?></li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
        <?php
        }
        ?>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="reviews" role="tabpanel" aria-labelledby="reviews">
                        <div class="row">
                            <div class="col-12">
                                <h6 class="lis-font-weight-500"><i class="fa fa-signal pr-2 lis-f-14"></i> Reviews</h6> </div>
                        </div>
                        <div class="row portfolio-box2">
                            
                        <?php
                            while ($row2 = mysqli_fetch_array($list2)){
                        ?>
                            <div class="post col-12 col-md-6 wow fadeInUp mb-4 mb-lg-0">
                                <div class="card lis-brd-light">
                                    <div class="card-body p-4">
                                        <div class="media d-block d-sm-flex text-center text-sm-left">
                                            
                                            <div class="media-body align-self-center">
                                                <p><?php echo $row2['time']; ?></p>
                                                <h6 class="lis-primary mb-2"><?php echo $row2['userMessage']; ?></h6>
                                                <p>Reply From RE: <?php echo $row2['reply']; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php
                            }
                        ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Profile Content -->
        <!-- Footer-->
<section class="image-bg footer lis-grediant grediant-bt pb-0">
            <div class="background-image-maker"></div>
            <div class="holder-image"> <img src="dist/images/bg3.jpg" alt="" class="img-fluid d-none"> </div>
            <div class="container">
                <div class="row pb-5">
                    <div class="col-12 col-md-8">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                                <h5 class="footer-head">Useful Links</h5>
                                <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li>
                                        <A href="add-place-listing.php"><i class="fa fa-angle-right pr-1"></i> Add Listing</A>
                                    </li>
                                    <li>
                                        <A href="loginhead.php"><i class="fa fa-angle-right pr-1"></i> Sign In</A>
                                    </li>
                                    <li>
                                        <A href="signuphead.php"><i class="fa fa-angle-right pr-1"></i> Register</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Contact Us</A>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                                <h5 class="footer-head">My Account</h5>
                                <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li>
                                        <A href="user-profile.php"><i class="fa fa-angle-right pr-1"></i> Dashboard</A>
                                    </li>
                                    <li>
                                        <A href="user-profile.php#listing"><i class="fa fa-angle-right pr-1"></i> My Listing</A>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-md-0">
                                <!-- <h5 class="footer-head">Pages</h5> -->
                                <!-- <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Blog</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Our Partners</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> How It Work</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Privacy Policy</A>
                                    </li>
                                </ul> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="footer-logo">
                            <a href="#"><img src="dist/images/logo-v1.png" alt="" class="img-fluid" /></a>
                        </div>
                        <p class="my-4">Rental Express</p> <a href="#" class="text-white"></a>
                    </div>
                </div>
            </div>
        </section>
        <!--End  Footer-->
        <!-- Top To Bottom-->
        <a href="#" class="scrollup text-center lis-bg-primary lis-rounded-circle-50">
            <div class="text-white mb-0 lis-line-height-1_7 h3"><i class="icofont icofont-long-arrow-up"></i></div>
        </a>
        <!-- End Top To Bottom-->
        <!-- Login /Register Form-->
        <div class="container">


            <div id="modal" class="popupContainer" style="display: none;">
                <header class="popupHeader">
                    <span class="header_title">Login</span>
                    <span class="modal_close"><i class="fa fa-times"></i></span>
                </header>

                <div class="popupBody">
                    <!-- Social Login -->						

                    <!-- Username & Password Login form -->
                    <div class="user_login">
                        <form action="login.php" method="POST">

                            <input type="email" class="form-control border-top-0 border-left-0 border-right-0 rounded-0 " placeholder="Email address" name="email" />
                            <br />


                            <input type="password" class="form-control border-top-0 border-left-0 border-right-0 rounded-0 " placeholder="password" name="password" />
                            <br />

                            <div class="checkbox">
                                <input id="remember" type="checkbox" />
                                <label for="remember">Remember me on this computer</label>
                            </div>

                            <div class="action_btns">
                                <input class="btn btn-primary btn-default mt-3 w-100" type="Submit" name="login-btn" value="Login"> 
                               <!--  <a href="#" class="btn btn-primary btn-default mt-3 w-100">Login</a> -->
                            </div>
                        </form >
                        <br/>
                        Sign in with your social network<br/>
                        <ul class="list-inline my-0">
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-facebook"></i></a></li>
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-twitter"></i></a></li>
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-linkedin"></i></a></li>
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-tumblr"></i></a></li>
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-pinterest-p"></i></a></li>
                        </ul>
                        <hr/>
                        Don't have an account <a href="#" class="register_form">Sign Up</a>
                    </div>

                    <!-- Register Form -->
                    <div class="user_register">
                        <form action="register.php">

                            <input type="text" class="form-control border-top-0 border-left-0 border-right-0 rounded-0" placeholder="Username" name="UserName" />
                            <br />


                            <input type="email" class="form-control border-top-0 border-left-0 border-right-0 rounded-0" placeholder="Email Address" name="email" />
                            <br />


                            <input type="password" class="form-control border-top-0 border-left-0 border-right-0 rounded-0" placeholder="Password" name="password" />
                            <br />

                             <input type="password" class="form-control border-top-0 border-left-0 border-right-0 rounded-0" placeholder="Confirm Password" />
                            <br />

                            <div class="checkbox">
                                <input id="send_updates" type="checkbox" />
                                <label for="send_updates">Send me occasional email updates</label>
                            </div>

                            <div class="action_btns">
                                <input class="btn btn-primary btn-default mt-3 w-100" type="submit" name="register-btn" value="Register">
                               <!--  <a href="#" class="btn btn-primary btn-default mt-3 w-100">Register</a> -->                            </div>
                        </form>
                        <br/>
                        Register with your social network<br/>
                        <ul class="list-inline my-0">
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-facebook"></i></a></li>
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-twitter"></i></a></li>
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-linkedin"></i></a></li>
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-tumblr"></i></a></li>
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-pinterest-p"></i></a></li>
                        </ul>
                        <hr/>
                        Already have an account <a href="#" class="login_form">Sign In</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Login /Register Form-->
        <!-- jQuery -->
        <script src="dist/js/plugins.min.js"></script>
        <script src="dist/js/common.js"></script>
    </body>



</html>