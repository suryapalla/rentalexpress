<?php
    session_start();
    $category = $_GET['category'];
    require 'db.php';
    require 'Category.php';
    $cat = new Category;
    $catList = $cat->getList($con);
    if (isset($_GET['item']) && $category == 'All Categories') {
    $list = $cat->getRentersItems($con,$_GET['item']);       
    }
    else if (isset($_GET['item'])) {
    $list = $cat->getRentersCatItems($con,$category,$_GET['item']);
    unset($_GET['item']);      
    }
    else{
    $list = $cat->getRenters($con,$category);
    }
    $message='';
    $errorMessage='';
    if (isset($_SESSION['message'])) {
        $errorMessage = $_SESSION['message'];
        $_SESSION['message']='';
    }
    if (isset($_SESSION['userDetails'])) {
        $message='Success';
    }
?>
<!DOCTYPE html>
<html lang="en">


<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Rental Express</title>
        <link rel="shortcut icon" href="dist/images/favicon.ico">
        <!--Plugin CSS-->
        <link href="dist/css/plugins.min.css" rel="stylesheet">
        <!--main Css-->
        <link href="dist/css/main.min.css" rel="stylesheet"> 
    </head>

    <body>
        <!-- header -->
        <div id="header-fix" class="header fixed-top">
            <nav class="navbar navbar-toggleable-md navbar-expand-lg navbar-light py-lg-0 py-4">
                <a class="navbar-brand mr-4 mr-md-5" href="index.php"><img src="dist/images/logo-v1.png" alt=""></a>
                <div id="dl-menu" class="dl-menuwrapper d-block d-lg-none float-right">
                    <button>Open Menu</button>
                    <ul class="dl-menu">

                        <li class="nav-item active">
                            <a class="nav-link" href="index.php" aria-expanded="false">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="listing-categories-style2.php" aria-expanded="false">Explore</a>
                        </li>
                        
                       <?php
                            if ($message=='Success') {
                        ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Pages</a>
                            <ul class="dl-submenu">
                                <li class="dl-back"><a href="#">back</a></li>
                            <li><a href="user-profile.php"> User Profile</a></li>
                            <li><a href="log-out.php" class="fa fa-sign-out">Log-out</a></li>
                            </ul>
                        </li>
                        <?php
                            }
                        ?>
                            


                <?php
                    if ($message!='Success') {
                ?>

                    <ul class="list-unstyled my-2 my-lg-0">
                        <li>
                             <a href="loginhead.php" class="text-white"><i class="fa fa-lock pr-2"></i> Sign In</a>
                        </li>
                    </ul>
                     
                <?php
                    }
                else{
                ?> 

                    <ul class="list-unstyled my-2 my-lg-0">
                        <li>
                             <a href="user-profile.php" class="text-white"><i class="fa fa-user-circle-o pr-2"></i>Welcome <?php echo$_SESSION['userDetails']['userName'];?></a>
                        </li>
                    </ul>
                        <li> <a href="add-place-listing.php" ><i class="fa fa-plus pr-1"></i> Add Listing</a></li>
                <?php
                    }
                ?>



                    </ul>
                </div>

                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active"> <a class="nav-link" href="index.php" aria-expanded="false">Home</a>
                        </li>
                        <li class="nav-item dropdown"> <a class="nav-link" href="#" aria-expanded="false">Explore</a>
                            <ul class="dropdown-menu">
                                <li><a href="listing-explore-place-profile.html">Explore Place</a></li>
                                <li><a href="listing-explore-event-profile.html">Explore Event</a></li>
                                <li><a href="listing-explore-property-profile.html">Explore Property</a></li>
                            </ul>
                        </li>
                        
                        <?php
                            if ($message=='Success') {
                        ?>
                        <li class="nav-item dropdown"> <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Pages <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="user-profile.php"> User Profile</a></li>
                                <li><a href="log-out.php" class="fa fa-sign-out">Log-out</a></li>
                                </ul>
                        </li>
                        <?php
                            }
                        ?>
                            
                    </ul>
                <?php
                    if ($message!='Success') {
                ?>

                    <ul class="list-unstyled my-2 my-lg-0">
                        <li>
                             <a href="loginhead.php" class="text-white"><i class="fa fa-lock pr-2"></i> Sign In</a>
                        </li>
                    </ul>
                     
                <?php
                    }
                else{
                ?> 

                    <ul class="list-unstyled my-2 my-lg-0">
                        <li>
                             <a href="user-profile.php" class="text-white"><i class="fa fa-user-circle-o pr-2"></i>Welcome <?php echo$_SESSION['userDetails']['userName'];?></a>
                        </li>
                    </ul>
                                        <a href="add-place-listing.php" class="btn btn-outline-light btn-sm ml-0 ml-lg-4 mt-3 mt-lg-0"><i class="fa fa-plus pr-1"></i> Add Listing</a> </div>
                <?php
                    }
                ?>
            </nav>
        </div>
        <!--End header -->
        <!-- Sidebar -->
        <section class="">
            <div class="container">
                <div class="row pt-5">
                    <div class="col-12 col-md-12">
                        <div class="lis-relative">
                        <form action="listing-no-map-fullwidth.php" method="GET">
                            <h5 class="mb-2">What are you looking for?</h5>
                            <p>Search by Category</p>
                            <div class="row">
                                <div class="col-12 col-sm-6 col-lg-3">
                                    <div class="form-group lis-relative">
                                        <input type="text" class="form-control border-top-0 border-left-0 border-right-0 rounded-0 pl-4" placeholder="What are you looking for?" required name="item">
                                        <div class="lis-search"> <i class="fa fa-search lis-primary lis-left-0"></i> </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-lg-3">
                                    <div class="form-group lis-relative">
                                        <select name="category" class="style-select form-control border-top-0 border-left-0 border-right-0 rounded-0 pl-4">
                                            <option> All Categories</option>
            <?php
                while ($names = mysqli_fetch_array($catList) ) {
            ?>
                                                <option><?php echo $names['name'];?></option>
            <?php
                }
            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row my-4">
                                <div class="col-12 col-md-12">
                                    <input class="btn btn-primary btn-default" type="submit" name="" value="Search">
                                </div>
                            </div>
                        </form>
                            <div class="row">
                                <div class="col-sm-6 align-self-center">
                                    <p>
                                        <?php
                                            if ($list->num_rows == 0) {
                                                echo "no results found";
                                            }
                                            else{
                                                echo "Showing results 1 - 6";
                                            }
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
        <?php
            while ($row=mysqli_fetch_array($list)) {
        ?>
                                <div class="col-12 col-md-6 col-lg-6 col-xl-4 mb-4">
                                    <div class="card lis-brd-light wow fadeInUp text-center text-lg-left">
                                        <a class="clickon" href="booking.php?item=<?php echo $row['increment'];?>">
                                            <div class="lis-grediant grediant-tb-light2 lis-relative modImage lis-radius rounded-top"> <img src="<?php echo $row['imagePath'];?>" alt="" class="img-fluid rounded-top w-100" /> </div>
                                            <div class="lis-absolute lis-right-20 lis-top-20">
                                                <div class="lis-post-meta border border-white text-white rounded lis-f-14">Book-it Now</div>
                                            </div>
                                        </a>
                                        <div class="card-body pt-0">
                                            <div class="media d-block d-lg-flex lis-relative">
                                                <div class="media-body align-self-start mt-2">
                                                    <h6 class="mb-0 lis-font-weight-600"><A href="#" class="lis-dark"><?php echo $row['title'];?></A></h6> </div>
                                            </div>
                                            <ul class="list-unstyled my-4 lis-line-height-2">
                                                <li><i class="fa fa-phone pr-2"></i> +<?php echo $row['mobileNumber'];?></li>
                                                <li><i class="fa fa-map-o pr-2"></i><?php echo $row['address'];?></li>
                                            </ul>
                                            
                                        </div>
                                    </div>
                                </div>
        <?php
            }
        ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End Sidebar -->
        <!-- Footer-->
<section class="image-bg footer lis-grediant grediant-bt pb-0">
            <div class="background-image-maker"></div>
            <div class="holder-image"> <img src="dist/images/bg3.jpg" alt="" class="img-fluid d-none"> </div>
            <div class="container">
                <div class="row pb-5">
                    <div class="col-12 col-md-8">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                                <h5 class="footer-head">Useful Links</h5>
                                <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li>
                                        <A href="add-place-listing.php"><i class="fa fa-angle-right pr-1"></i> Add Listing</A>
                                    </li>
                                    <li>
                                        <A href="loginhead.php"><i class="fa fa-angle-right pr-1"></i> Sign In</A>
                                    </li>
                                    <li>
                                        <A href="signuphead.php"><i class="fa fa-angle-right pr-1"></i> Register</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Contact Us</A>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                                <h5 class="footer-head">My Account</h5>
                                <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li>
                                        <A href="user-profile.php"><i class="fa fa-angle-right pr-1"></i> Dashboard</A>
                                    </li>
                                    <li>
                                        <A href="user-profile.php#listing"><i class="fa fa-angle-right pr-1"></i> My Listing</A>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-md-0">
                                <!-- <h5 class="footer-head">Pages</h5> -->
                                <!-- <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Blog</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Our Partners</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> How It Work</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Privacy Policy</A>
                                    </li>
                                </ul> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="footer-logo">
                            <a href="#"><img src="dist/images/logo-v1.png" alt="" class="img-fluid" /></a>
                        </div>
                        <p class="my-4">Rental Express</p> <a href="#" class="text-white"></a>
                    </div>
                </div>
            </div>
        </section>
        <!--End  Footer-->
        <!-- Top To Bottom-->
        <a href="#" class="scrollup text-center lis-bg-primary lis-rounded-circle-50">
            <div class="text-white mb-0 lis-line-height-1_7 h3"><i class="icofont icofont-long-arrow-up"></i></div>
        </a>
        <!-- End Top To Bottom-->
        <!-- jQuery -->
        <script src="dist/js/plugins.min.js"></script>
        <script src="dist/js/common.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                <?php
                    if (!isset($_SESSION['userDetails'])) {
                ?>
                    $(".clickon").click(function() {
                        alert("Please login before bookings");
                    })
                <?php
                    }
                ?>
            });
        </script>

    </body>


<!-- Mirrored from psd2allconversion.com/templates/themeforest/html/lister/default/listing-no-map-fullwidth.php by HTTrack Website Copier/3.x [XR&CO'2017], Sat, 20 Jan 2018 16:03:59 GMT -->
</html>