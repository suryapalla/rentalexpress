<?php
    session_start();
        $message='';
    $errorMessage='';
    require 'db.php';
    if (isset($_SESSION['message'])) {
        $errorMessage = $_SESSION['message'];
        $_SESSION['message']='';
    }
    if (isset($_SESSION['userDetails'])) {
        $message='Success';
    }

    if (isset($_POST['verify-btn'])) {
        if ($_POST['otp'] == $_SESSION['otp']) {
            $userId = $_SESSION['userId'];
            $query  = "UPDATE users SET status = '1' WHERE userId = '$userId'";
            $result = $con->query($query);
            if ($result) {
           $_SESSION['message'] = '<p style="color:#ff214f;font-size:13px;margin:0px;">You have successfully registered.</p>';
            header("location:signuphead.php");
            }
        }
        else{
            $_SESSION['message'] = '<p style="color:#ff214f;font-size:13px;margin:0px;">Please enter valid OTP</p>';
            echo "not equal";
            die();
        }
    }
?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Rental Express</title>
        <link rel="shortcut icon" href="dist/images/favicon.ico">
        <!--Plugin CSS-->
        <link href="dist/css/plugins.min.css" rel="stylesheet">
        <!--main Css-->
        <link href="dist/css/main.min.css" rel="stylesheet"> 
        <style type="text/css">
            #submit-btn{
                cursor: pointer;
            }
        </style>
</head>
<body>
        <div id="header-fix" class="header fixed-top">
            <nav class="navbar navbar-toggleable-md navbar-expand-lg navbar-light py-lg-0 py-4">
                <a class="navbar-brand mr-4 mr-md-5" href="index.php"><img src="dist/images/logo-v1.png" alt=""></a>
                <div id="dl-menu" class="dl-menuwrapper d-block d-lg-none float-right">
                    <button>Open Menu</button>
                    <ul class="dl-menu">

                        <li class="nav-item active dropdown">
                            <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Home</a>
                            <ul class="dl-submenu">
                                <li class="dl-back"><a href="#">back</a></li>
                                <li><a href="index.php">Home 1</a></li>
                                <li><a href="index.php">Home 2</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Explore</a>
                            <ul class="dl-submenu">
                                <li class="dl-back"><a href="#">back</a></li>
                                <li><a href="listing-explore-place-profile.html">Explore Place</a></li>
                                <li><a href="listing-explore-event-profile.html">Explore Event</a></li>
                                <li><a href="listing-explore-property-profile.html">Explore Property</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Listing</a>
                            <ul class="dl-submenu">
                                <li class="dl-back"><a href="#">back</a></li>
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">Listing By Categories</a>
                                    <ul class="dl-submenu">
                                        <li class="dl-back"><a href="#">back</a></li>
                                        <li><a href="listing-categories-style1.html">Category 1</a></li>
                                        <li><a href="listing-categories-style2.php">Category 2</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">Listing No Map</a>
                                    <ul class="dl-submenu">
                                        <li class="dl-back"><a href="#">back</a></li>
                                        <li><a href="listing-no-map-sidebar.html">With Sidebar</a></li>
                                        <li><a href="listing-no-map-fullwidth.php">Full Width</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">Half Screen Map</a>
                                    <ul class="dl-submenu">
                                        <li class="dl-back"><a href="#">back</a></li>
                                        <li><a href="half-screen-search-sidebar-place.html">With Search Place</a></li>
                                        <li><a href="half-screen-search-sidebar-event.html">With Search Event</a></li>
                                        <li><a href="half-screen-search-sidebar-property.html">With Search Property</a></li>
                                        <li><a href="half-screen-with-categories-sidebar.html">With Categories Sidebar</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Pages</a>
                            <ul class="dl-submenu">
                                <li class="dl-back"><a href="#">back</a></li>
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">Blog</a>
                                    <ul class="dl-submenu">
                                        <li class="dl-back"><a href="#">back</a></li>
                                        <li><a href="blog-grid.html"> Blog Grid</a></li>
                                        <li><a href="blog-listing.html"> Blog Listing</a></li>
                                        <li><a href="blog-single.html"> Single Post</a></li>
                                    </ul>
                                </li>
                                <li><a href="contact.html"> Contact</a></li>
                                <li><a href="user-profile.php"> User Profile</a></li>
                                <li><a href="faq.html"> Faq</a></li>
                       <?php
                            if ($message=='Success') {
                        ?>
                            <li><a href="log-out.php" class="fa fa-sign-out">Log-out</a></li>
                        <?php
                            }
                        ?>
                                <li><a href="coming-soon.html"> Coming Soon</a></li>
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">404 Error</a>
                                    <ul class="dl-submenu">
                                        <li class="dl-back"><a href="#">back</a></li>
                                        <li><a href="error-dark.php"> 404 Error Dark</a></li>
                                        <li><a href="error-light.html"> 404 Error Light</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>


                <?php
                    if ($message!='Success') {
                ?>

                    <ul class="list-unstyled my-2 my-lg-0">
                        <li>
                             <a href="#modal" class="text-white login_form"><i class="fa fa-lock pr-2"></i> Sign In | Register</a>
                        </li>
                    </ul>
                     
                <?php
                    }
                else{
                ?> 

                    <ul class="list-unstyled my-2 my-lg-0">
                        <li>
                             <a href="user-profile.php" class="text-white"><i class="fa fa-user-circle-o pr-2"></i>Welcome <?php echo$_SESSION['userDetails']['userName'];?></a>
                        </li>
                    </ul>
                        <li> <a href="add-place-listing.php" ><i class="fa fa-plus pr-1"></i> Add Listing</a></li>
                <?php
                    }
                ?>



                    </ul>
                </div>

                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active dropdown"> <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Home <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="index.php">Home 1</a></li>
                                <li><a href="index.php">Home 2</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown"> <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Explore <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="listing-explore-place-profile.html">Explore Place</a></li>
                                <li><a href="listing-explore-event-profile.html">Explore Event</a></li>
                                <li><a href="listing-explore-property-profile.html">Explore Property</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown"> <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Listing <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">Listing By Categories</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="listing-categories-style1.html">Category 1</a></li>
                                        <li><a href="listing-categories-style2.php">Category 2</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">Listing No Map</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="listing-no-map-sidebar.html">With Sidebar</a></li>
                                        <li><a href="listing-no-map-fullwidth.php">Full Width</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">Half Screen Map</a>
                                     <ul class="dropdown-menu">
                                        <li><a href="half-screen-search-sidebar-place.html">With Search Place</a></li>
                                        <li><a href="half-screen-search-sidebar-event.html">With Search Event</a></li>
                                        <li><a href="half-screen-search-sidebar-property.html">With Search Property</a></li>
                                        <li><a href="half-screen-with-categories-sidebar.html">With Categories Sidebar</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown"> <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="false">Pages <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">Blog</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="blog-grid.html"> Blog Grid</a></li>
                                        <li><a href="blog-listing.html"> Blog Listing</a></li>
                                        <li><a href="blog-single.html"> Single Post</a></li>
                                    </ul>
                                </li>
                                <li><a href="contact.html"> Contact</a></li>
                                <li><a href="user-profile.php"> User Profile</a></li>
                                <li><a href="faq.html"> Faq</a></li>
                        <?php
                            if ($message=='Success') {
                        ?>
                                <li><a href="log-out.php" class="fa fa-sign-out">Log-out</a></li>
                        <?php
                            }
                        ?>
                                <li><a href="coming-soon.html"> Coming Soon</a></li>
                                <li class="dropdown-submenu"><a tabindex="-1" href="#">404 Error</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="error-dark.php"> 404 Error Dark</a></li>
                                        <li><a href="error-light.html"> 404 Error Light</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                <?php
                    if ($message!='Success') {
                ?>

                    <ul class="list-unstyled my-2 my-lg-0">
                        <li>
                             <a href="#modal" class="text-white login_form"><i class="fa fa-lock pr-2"></i> Sign In | Register</a>
                        </li>
                    </ul>
                     
                <?php
                    }
                else{
                ?> 

                    <ul class="list-unstyled my-2 my-lg-0">
                        <li>
                             <a href="user-profile.php" class="text-white"><i class="fa fa-user-circle-o pr-2"></i>Welcome <?php echo$_SESSION['userDetails']['userName'];?></a>
                        </li>
                    </ul>
                                        <a href="add-place-listing.php" class="btn btn-outline-light btn-sm ml-0 ml-lg-4 mt-3 mt-lg-0"><i class="fa fa-plus pr-1"></i> Add Listing</a> </div>
                <?php
                    }
                ?>
            </nav>
        </div>
        <!--End header -->
        <div id="modal" class="popupContainer" style="display: block; opacity: 1; z-index: 1020; left: 50%; margin-left: -165px; top: 120px;">
                <header class="popupHeader">
                    <span class="header_title">OTP</span>
                    <span class="modal_close"><i class="fa fa-times"></i></span>
                </header>

                <div class="popupBody">
                    <!-- Register Form -->
                    <div>
                        <form action="verify.php" method="POST">

                            <input type="number" class="form-control border-top-0 border-left-0 border-right-0 rounded-0" placeholder="Enter OTP" name="otp" />
                            <br />

                            <div class="action_btns">
                                <input id="submit-btn" class="btn btn-primary btn-default mt-3 w-100" type="submit" name="verify-btn" value="Register">
                            </div>
                        </form>
                        <br/>
                        Register with your social network<br/>
                        <ul class="list-inline my-0">
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-facebook"></i></a></li>
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-twitter"></i></a></li>
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-linkedin"></i></a></li>
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-tumblr"></i></a></li>
                            <li class="list-inline-item mr-0"><a href="#" class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i class="fa fa-pinterest-p"></i></a></li>
                        </ul>
                        <hr/>
                        Already have an account <a href="#">Sign In</a>
                    </div>
                    </div>

            </div>
         <!--popup ended-->
<div style="height: 870px;"></div>
        <!-- Footer-->
        <section class="image-bg footer lis-grediant grediant-bt pb-0">
            <div class="background-image-maker"></div>
            <div class="holder-image"> <img src="dist/images/bg3.jpg" alt="" class="img-fluid d-none"> </div>
            <div class="container">
                <div class="row pb-5">
                    <div class="col-12 col-md-8">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                                <h5 class="footer-head">Useful Links</h5>
                                <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Add Listing</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Sign In</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Register</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Pricing</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Contact Us</A>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-lg-0">
                                <h5 class="footer-head">My Account</h5>
                                <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Dashboard</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Profile</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> My Listing</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Favorites</A>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3 mb-4 mb-md-0">
                                <h5 class="footer-head">Pages</h5>
                                <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Blog</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Our Partners</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> How It Work</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Privacy Policy</A>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-6 col-lg-3">
                                <h5 class="footer-head">Help</h5>
                                <ul class="list-unstyled footer-links lis-line-height-2_5">
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Add Listing</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Sign In</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Register</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Pricing</A>
                                    </li>
                                    <li>
                                        <A href="#"><i class="fa fa-angle-right pr-1"></i> Contact Us</A>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="footer-logo">
                            <a href="#"><img src="dist/images/logo-v1.png" alt="" class="img-fluid" /></a>
                        </div>
                        <p class="my-4">Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu in felis eu pede mollis enim.</p> <a href="#" class="text-white"><u>App Download</u></a>
                        <ul class="list-inline mb-0 mt-2">
                            <li class="list-inline-item">
                                <A href="#"><img src="dist/images/play-store.png" alt="" class="img-fluid" /></a>
                            </li>
                            <li class="list-inline-item">
                                <A href="#"><img src="dist/images/google-play.png" alt="" class="img-fluid" /></a>
                            </li>
                            <li class="list-inline-item">
                                <A href="#"><img src="dist/images/windows.png" alt="" class="img-fluid" /></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-bottom mt-5 py-4">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-md-6 text-center text-md-left mb-3 mb-md-0"> <span> &copy; 2017 Lister. Powered by <a href="#" class="lis-primary">PSD2allconversion.</a></span> </div>
                        <div class="col-12 col-md-6 text-center text-md-right">
                            <ul class="list-inline footer-social mb-0">
                                <li class="list-inline-item pr-3">
                                    <A href="#"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li class="list-inline-item pr-3">
                                    <A href="#"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li class="list-inline-item pr-3">
                                    <A href="#"><i class="fa fa-linkedin"></i></a>
                                </li>
                                <li class="list-inline-item pr-3">
                                    <A href="#"><i class="fa fa-tumblr"></i></a>
                                </li>
                                <li class="list-inline-item">
                                    <A href="#"><i class="fa fa-pinterest-p"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--End  Footer-->
        <!-- Top To Bottom-->
        <a href="#" class="scrollup text-center lis-bg-primary lis-rounded-circle-50">
            <div class="text-white mb-0 lis-line-height-1_7 h3"><i class="icofont icofont-long-arrow-up"></i></div>
        </a>
        <!-- End Top To Bottom-->

        <!-- jQuery -->
        <script src="dist/js/plugins.min.js"></script>
        <script src="dist/js/common.js"></script>
</body>
</html>